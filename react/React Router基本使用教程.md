## 简介

`React Router` 是完整的` React `路由解决方案。

它通过管理 URL，实现组件的切换和状态的变化，开发复杂的应用几乎肯定会用到。

路由维护了URL地址和组件的映射关系, 通过这个映射关系,

我们就可以根据不同的URL地址，去渲染不同的组件

注意：

`react-router4`之前, 所有路由代码都是统一放到`react-router`中管理的

`react-router4`开始, 拆分为了两个包`react-router-dom`和`react-router-native`

+ `react-router-dom `在浏览器中使用路由
+ `react-router-native `在原生应用中使用路由

`BrowserRouter history`模式使用的是H5的特性, 所以兼容性会比`HashRouter hash`模式差一些

在企业开发中如果不需要兼容低级版本浏览器, 建议使用`BrowserRouter`

如果需要兼容低级版本浏览器, 那么只能使用`HashRouter`

## 简单使用

安装`Router`

> npm install react-router-dom --save

通过指定监听模式

`BrowserRouter   history`模式 	/home

`HashRouter   hash`模式	/#/home

通过`Link`修改路由`URL`地址

通过`Route`匹配路由地址

```javascript
1.BrowserRouter和HashRouter作用:
指定路由的监听模式 history模式 / hash模式

import {BrowserRouter, HashRouter, Link, Route} from 'react-router-dom';

<HashRouter>
	{/*
    2.Link作用:
    用于修改URL的资源地址
    */}
    <Link to={'/home'}>Home</Link>
    <Link to={'/about'}>About</Link>
    {/*
    3.Route作用:
    用于维护URL和组件的关系
    Route是一个占用组件, 将来它会根据匹配到的资源地址渲染对应的组件
    */}
	{
        /*
        注意：
            默认情况下Route在匹配资源地址时, 是模糊匹配
            如果必须和资源地址一模一样才匹配, 那么需要添加exact属性, 开启精准匹配
        */
    }
    <Route path={'/home'} component={Home}/>
    <Route path={'/about'} component={About}/>
</HashRouter>


NavLink注意点：
    默认情况下NavLink在匹配资源地址时, 是模糊匹配
    如果必须和资源地址一模一样才匹配, 那么需要添加exact属性, 开启精准匹配
    NavLink在匹配路由的时候, 是利用当前资源地址从左至右的和path中的地址进行匹配的
    只要当前资源地址从左至右完整的包含了path中的地址那么就认为匹配
    当前资源地址 : /home/about
    to中的地址: /home
    to中的地址: /home/about
    
Route注意点:
    Route在匹配路由的时候, 是利用当前资源地址从左至右的和path中的地址进行匹配的
    只要当前资源地址从左至右完整的包含了path中的地址那么就认为匹配
    当前资源地址 : /home/about
    path中的地址: /home
    path中的地址: /home/about

```

**注意：无论是Link还是Route都只能放到BrowserRouter和HashRouter中才有效**

**Switch**

```javascript
1.Switch:
默认情况下路由会从上至下匹配所有的Route, 只要匹配都会显示
但是在企业开发中大部分情况下, 我们希望的是一旦有一个匹配到了后续就不要再匹配了
此时我们就可以通过Switch来实现

<BrowserRouter>
    <Link to={'/home'}>Home</Link>
    <Link to={'/about'}>About</Link>

    <Switch>
        <Route exact path={'/home'} component={Home}/>
        <Route exact path={'/about'} component={About}/>
        {/*如果Route没有指定path, 那么表示这个Route和所有的资源地址都匹配*/}
        <Route component={Other}/>
    </Switch>
</BrowserRouter>
```

**Redirect**

```javascript
1.Redirect:
资源重定向, 也就是可以在访问某个资源地址的时候重定向到另外一个资源地址
例如: 访问/user 重定向到 /login
<Redirect to={'/login'}/>
```


**嵌套路由**

```javascript
1.嵌套路由(子路由):
路由里面又有路由, 我们就称之为嵌套路由
<BrowserRouter>
    <NavLink to={'/home'} activeStyle={{color:'red'}}>Home</NavLink>
    <NavLink to={'/about'} activeStyle={{color:'red'}}>About</NavLink>
    <NavLink to={'/user'} activeStyle={{color:'red'}}>User</NavLink>
    <NavLink to={'/discover'} activeStyle={{color:'red'}}>广场</NavLink>

    <Switch>
        <Route exact path={'/home'} component={Home}/>
        <Route exact path={'/about'} component={About}/>
        <Route exact path={'/user'} component={User}/>
        <Route exact path={'/login'} component={Login}/>
        {/*
        注意点:如果要使用嵌套路由, 那么外层路由不能添加精准匹配exact
        /discover/toplist
        /discover
        */}
        <Route path={'/discover'} component={Discover}/>
        <Route component={Other}/>
    </Switch>
</BrowserRouter>

 /*
注意点: 由于当前组件是在BrowserRouter or HashRouter中显示的
       所以在当前组件中不用使用BrowserRouter or HashRouter来包裹NavLink/Switch/Route
* */
<div>
    <NavLink exact to={'/discover'} activeStyle={{color:'red'}}>推荐</NavLink>
    <NavLink exact to={'/discover/toplist'} activeStyle={{color:'red'}}>排行榜</NavLink>
    <NavLink exact to={'/discover/playlist'} activeStyle={{color:'red'}}>歌单</NavLink>

    <Switch>
        <Route exact path={'/discover'} component={Hot}/>
        <Route exact path={'/discover/toplist'} component={TopList}/>
        <Route exact path={'/discover/playlist'} component={PlayList}/>
    </Switch>
</div>

```

**手动路由跳转**

```javascript
1.手动路由跳转:
不通过Link/NavLink来设置资源地址, 而是通过JS来设置资源地址
        
// 如果是Hash模式, 那么只需要通过JS设置Hash值即可
// window.location.hash = '/discover/playlist';
// 如果一个组件是通过路由创建出来的, 那么系统就会自动传递一个history给我们
// 我们只需要拿到这个history对象, 调用这个对象的push方法, 通过push方法修改资源地址即可
// console.log(this.props.history);
this.props.history.push('/discover/playlist');

1.手动路由跳转注意点:
- 只有通过路由创建出来的组件才有history对象, 所以不能在根组件中使用手动路由跳转
- 如果想在根组件中使用手动路由跳转, 那么需要借助一个withRouter高阶组件

import {BrowserRouter, HashRouter, NavLink, Route, Switch, withRouter} from 'react-router-dom';
/*
    Cannot read property 'push' of undefined
    如果一个组件是通过路由创建的, 那么系统就会自动给这个组件传递一个history对象
    但是如果一个组件不是通过路由创建的, 那么系统就不会给这个组件传递一个history对象

    如果现在非路由创建出来的组件中使用history对象, 那么可以借助withRouter高阶组件
    只要把一个组件传递给withRouter方法, 那么这个方法就会通过路由将传入的组件创建出来

    注意点: 如果一个组件要使用路由创建, 那么这个组件必须包裹在BrowserRouter, HashRouter中
* */
export default withRouter(App);
```

**路由参数传递**

```javascript
1.路由参数传递
- URL参数
?key=value&key=value
<NavLink to={'/home?name=lnj&age=18'} activeStyle={{color:'red'}}>Home</NavLink>

// console.log(this.props.location);
// console.log(this.props.location.search);


- 路由参数(动态路由)
/path/:key
<NavLink to={'/about/lnj/18'} activeStyle={{color:'red'}}>About</NavLink>
<Switch>
	<Route exact path={'/about/:name/:age'} component={About}/>
</Switch>

console.log(this.props.match.params);


- 对象
<NavLink to={{
    pathname: "/user",
    search: "",
    hash: "",
    state: obj
}} activeStyle={{color:'red'}}>User</NavLink>

console.log(this.props.location.state);
```

**路由统一管理**

```javascript
1.路由统一管理(路由集中管理)
现在虽然我们能通过路由实现组件切换, 但是现在我们的路由都比较分散, 不利于我们管理和维护
所以React也考虑到了这个问题, 也给我们提供了统一管理路由的方案
https://www.npmjs.com/package/react-router-config

npm install --save react-router-config

import {renderRoutes} from 'react-router-config';
import routers from './router/index';

<div>
    <NavLink to={'/home?name=lnj&age=18'} activeStyle={{color:'red'}}>Home</NavLink>
    <NavLink to={'/about/lnj/18'} activeStyle={{color:'red'}}>About</NavLink>
    <NavLink to={{
        pathname: "/user",
        search: "",
        hash: "",
        state: obj
    }} activeStyle={{color:'red'}}>User</NavLink>
    <NavLink to={'/discover'} activeStyle={{color:'red'}}>广场</NavLink>
    <button onClick={()=>{this.btnClick()}}>广场</button>

    {/*
    <Switch>
        <Route exact path={'/home'} component={Home}/>
        <Route exact path={'/about/:name/:age'} component={About}/>
        <Route exact path={'/user'} component={User}/>
        <Route exact path={'/login'} component={Login}/>
        <Route path={'/discover'} component={Discover}/>
        <Route component={Other}/>
    </Switch>
    */}
    {renderRoutes(routers)}
</div>

./router/index.js
const routers = [
    {
        path:'/home',
        exact:true,
        component: Home
    },
    {
        path:'/about/:name/:age',
        exact:true,
        component: About
    },
    {
        path:'/user',
        exact:true,
        component: User
    },
    {
        path:'/login',
        exact:true,
        component: Login
    },
    {
        path:'/discover',
        exact:true,
        component: Discover
    },
    {
        component: Other
    },
];
export default routers;


//嵌套路由
{
    path:'/discover',
    component: Discover,
    routes:[
        {
            path:'/discover',
            exact:true,
            component: Hot,
        },
        {
            path:'/discover/toplist',
            exact:true,
            component: TopList,
        },
        {
            path:'/discover/playlist',
            exact:true,
            component: PlayList,
        },
    ]
},
//子路由
{renderRoutes(this.props.route.routes)}
```

