## 简介

`dva`是一个轻量级的应用框架

`dva`是一个基于 `redux` 和 `redux-saga` 的**数据流方案**

内置了`react`、`react-dom`、`react-router`、`redux`、`redux-saga`

## 基本使用

安装`dva`

>npm i dva --save

使用`dva`

```
+ 创建应用  let app = dva(opts);
+ 配置hooks或注册插件  app.use(hooks);
+ 注册model  app.model(model);
+ 注册路由表  app.router((history, app)=>{routerConfig});
+ 启动应用    app.start(selector?)
```

## 管理数据

## 异步处理

## Dva订阅

## Dva路由

## 其他