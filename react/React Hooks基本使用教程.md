## 简介

Hook 是 React 16.8 的新增特性，

它可以让函数式组件拥有类组件特性

在Hook出现之前, 如果我们想在组件中保存自己的状态,

 如果我们想在组件的某个生命周期中做一些事情, 那么我们必须使用类组件

+ 但是类组件的学习成本是比较高的, 你必须懂得ES6的class, 你必须懂得箭头函数

+ 但是在类组件的同一个生命周期方法中, 我们可能会编写很多不同的业务逻辑代码

   这样就导致了大量不同的业务逻辑代码混杂到一个方法中, 导致代码变得很难以维护

   (诸如: 在组件被挂载的生命周期中, 可能主要注册监听, 可能需要发送网络请求等)

+ 但是在类组件中共享数据是非常繁琐的, 需要借助`Context`或者`Redux`等

所以当应用程序变得复杂时, 类组件就会变得非常复杂, 非常难以维护

所以Hook就是为了解决以上问题而生的

Hook的使用我们无需额外安装任何第三方库, 因为它就是React的一部分

Hook只能在函数组件中使用, 不能在类组件，或者函数组件之外的地方使用

Hook只能在函数最外层调用, 不要在循环、条件判断或者子函数中调用

官方文档地址:：[https://react.docschina.org/docs/hooks-intro.html](https://react.docschina.org/docs/hooks-intro.html)

## `useState`

 参数: 保证状态的初始值

  返回值: 是一个数组, 这个数组中有两个元素

​      第一个元素: 保存的状态

​      第二个元素: 修改保存状态的方法

注意点: 在同一个函数式组件中, 是可以多次使用同名的Hook的

```javascript
const [ageState, setAgeState] = useState(18);
const [nameState, setNameState] = useState('lnj');
```

## `useEffect`

可以把 `useEffect Hook` 看做

`componentDidMount`，`componentDidUpdate` 和 `componentWillUnmount`

这三个生命周期函数的组合

注：`useEffect`中定义的函数的执行不会阻碍浏览器更新视图，也就是说这些函数时**异步执行**的

```javascript
//只要数据改变就会被调用
useEffect(()=>{
    // componentDidMount
    // componentDidUpdate
    console.log('组件被挂载或者组件更新完成');
    return ()=>{
        // componentWillUnmount
        console.log('组件即将被卸载');
    }
});

useEffect(()=>{
    // 组件被挂载
    console.log('修改DOM');
});
useEffect(()=>{
    // 组件被挂载
    console.log('注册监听');
    return ()=>{
        console.log('移出监听');
    }
});
useEffect(()=>{
    console.log('发送网络请求');
});
    
特点：
可以设置依赖, 只有依赖发生变化的时候才执行
const [nameState, setNameState] = useState('lnj');
const [ageState, setAgeState] = useState(0);
useEffect(()=>{
    // componentDidMount
    // componentDidUpdate
    console.log('组件被挂载或者组件更新完成');
    return ()=>{
        // componentWillUnmount
        console.log('组件即将被卸载');
    }
}, [nameState]);

[] --->  componentWillUnmount
```



## `useContext`

`useContext`相当于 类组件中的 `static contextType = Context`

```javascript
const UserContext = createContext({});
const ColorContext = createContext({});

function App() {
    return (
        <UserContext.Provider value={{name:'lnj', age:18}}>
            <ColorContext.Provider value={{color:'red'}}>
                <Home/>
            </ColorContext.Provider>
        </UserContext.Provider>
        )
}

-------------------------------------------------------
function Home() {
	return (
		<div>
            <p>{this.context.name}</p>
            <p>{this.context.age}</p>
        </div>
	)
}
Home.contextType = UserContext;

--------------------------------------------------------
function Home() {
	return (
		<UserContext.Consumer>
            {
                value1 =>{
                    return (
                        <ColorContext.Consumer>
                            {
                                value2 =>{
                                    return (
                                        <div>
                                            <p>{value1.name}</p>
                                            <p>{value1.age}</p>
                                            <p>{value2.color}</p>
                                        </div>
                                    )
                                }
                            }
                        </ColorContext.Consumer>
                    )
                }
            }
        </UserContext.Consumer>
	)
}

--------------------------------------------------------
function Home() {
    const user = useContext(UserContext);
    const color = useContext(ColorContext);
    return (
        <div>
            <p>{user.name}</p>
            <p>{user.age}</p>
            <p>{color.color}</p>
        </div>
    )
}
```



## `useReducer`

从名称来看, 很多人会误以为`useReducer`是用来替代`Redux`的, 但是其实不是

`useReducer`是`useState`的一种替代方案, 可以让我们很好的复用操作数据的逻辑代码

`useReducer`接收的参数:

    第一个参数: 处理数据的函数
    
    第二个参数: 保存的默认值

`useReducer`返回值:

会返回一个数组, 这个数组中有两个元素

    第一个元素: 保存的数据
    
    第二个元素: dispatch函数

```javascript
function reducer(state, action) {
    switch (action.type) {
        case 'ADD':
            return {...state, num: state.num + 1};
        case 'SUB':
            return {...state, num: state.num - 1};
        default:
            return state;
    }
}

const [state, dispatch] = useReducer(reducer, {num: 0});

dispatch({type:'ADD'})


*****************useContext+useReducer实现简单Redux************************
export const ColorContext = createContext({})

export const UPDATE_COLOR = "UPDATE_COLOR"

const reducer= (state,action)=>{
    switch(action.type){
        case UPDATE_COLOR:
            return action.color
        default:
            return state
    }
}


export const Color = props=>{
    const [color,dispatch]=useReducer(reducer,'blue')
    return (
        <ColorContext.Provider value={{color,dispatch}}>
            {props.children}
        </ColorContext.Provider>
    )
}


function Buttons(){
    const { dispatch } = useContext(ColorContext)
    return (
        <div>
            <button onClick={()=>{dispatch({type:UPDATE_COLOR,color:"red"})}}>红色</button>
            <button onClick={()=>{dispatch({type:UPDATE_COLOR,color:"yellow"})}}>黄色</button>
        </div>
    )
}
```



## `useCallback`

`useCallback`用于优化代码, 可以让对应的函数只有在依赖发生变化时才重新定义

```javascript
父组件中的数据发生了变化, 会重新渲染父组件
重新渲染父组件, 就会重新执行父组件函数
重新执行父组件函数, 就会重新定义increment/decrement
既然increment/decrement是重新定义的, 所以就和上一次的不是同一个函数了
既然不是同一个函数, 所以Home和About接收到的内容也和上一次的不一样了
既然接收到的内容和上一次不一样了, 所以就会重新渲染
    <MemoHome handler={increment}/>
    <MemoAbout handler={decrement}/>
    
    // 以下代码的作用: 只要countState没有发生变化, 那么useCallback返回的永远都是同一个函数
    const decrement = useCallback(()=>{
        setCountState(countState - 1);
    }, [countState]);
```



## `useMemo`

`useMemo`用于优化代码, 可以让对应的函数只有在依赖发生变化时才返回新的值

```javascript
    function useCallback(fn, arr){
        return useMemo(()=>{
            return fn;
        }, arr);
    }

    const decrement = useCallback(()=>{
        setCountState(countState - 1);
    }, [countState]);
    
    
    const user = useMemo(()=>{
        return {name: 'lnj', age:18};
    }, []);
```

**`useCallback`和`useMemo`区别:**

* `useCallback`返回的永远是一个函数

* `useMemo`返回的是return返回的内容



## `useRef`

`useRef`就是`createRef`的Hook版本, 只不过比`createRef`更强大一点。

注：不可用在函数组件中。

`createRef`和`useRef`区别:

* `useRef`除了可以用来获取元素以外, 还可以用来保存数据
* `useRef`中保存的数据, 除非手动修改, 否则永远都不会发生变化

```javascript
    // const pRef = createRef();
    // const homeRef = createRef();
    const pRef = useRef();
    const homeRef = useRef();
    function btnClick() {
        console.log(pRef.current);
        console.log(homeRef.current);
    }
    return (
        <div>
            <p ref={pRef}>我是段落</p>
            <Home ref={homeRef}/>
            <About/>
            <button onClick={()=>{btnClick()}}>获取</button>
        </div>
    )
    
**************************************************************

const age = useRef(numState);

******************forwardRef*********************************
在子组件转发暴露给父组件（可用在函数组件）

import React, {useRef, forwardRef, useImperativeHandle} from 'react';
function Home(props, appRef) {    
    return (
        <div>
            <p>Home</p>
            <input ref={appRef} type="text" placeholder={'请输入内容'}/>
        </div>
    )
}
const ForwardHome = forwardRef(Home);
function App() {
    const appRef = useRef();
    function btnClick() {
        console.log(appRef);
        console.log(appRef.current);
        appRef.current.focus();
        appRef.current.value = 'www.it666.com';
    }
    return (
        <div>
            <ForwardHome ref={appRef}/>
            <button onClick={()=>{btnClick()}}>获取</button>
        </div>
    )
}
export default App;
```

## `useImperativeHandle`

`useImperativeHandle`可以让你在使用 ref 时自定义暴露给父组件的实例值

```javascript
function Home(props, appRef) {
    const inputRef = useRef();
    useImperativeHandle(appRef, ()=>{
        return {	//appRef.current 
            myFocus: ()=>{
                inputRef.current.focus();
            }
        }
    });
    return (
        <div>
            <p>Home</p>
            <input ref={inputRef} type="text" placeholder={'请输入内容'}/>
        </div>
    )
}
const ForwardHome = forwardRef(Home);
function App() {
    const appRef = useRef();
    function btnClick() {
        appRef.current.myFocus();
    }
    return (
        <div>
            <ForwardHome ref={appRef}/>
            <button onClick={()=>{btnClick()}}>获取</button>
        </div>
    )
}
export default App;
```



## `useLayoutEffect`

 大部分情况下`useLayoutEffect`和`useEffect`没太大区别(用法格式都相同)

但是如果需要**修改DOM的布局样式**, 那么推荐使用`useLayoutEffect`

* `useEffect` 函数会在组件渲染到屏幕之后才执行, 所以会可能会出现闪屏情况

- `useLayoutEffect` 函数是在组件渲染到屏幕之前执行, 所以不会出现闪屏情况

**`useLayoutEffect`和`useEffect`区别：**

* 执行时机不同:
  * 如果是挂载或者更新组件, 那么`useLayoutEffect`会比`useEffect`先执行
  * 如果是卸载组件, 那么`useEffect`会比`useLayoutEffect`先执行
* `useEffect`: 同步
* `useLayoutEffect`: 异步

`useEffect`是组件已经渲染到屏幕上了才执行

`useLayoutEffect`是组件还没有渲染到屏幕上就会执行

所以如果在组件已经渲染到屏幕上了, 才去更新DOM的布局和样式, 那么用户体验不好, 会看到闪屏的情况

而如果是在组件还没有渲染到屏幕上, 就去更新DOM的布局和样式, 那么用户体验更好, 看不到闪屏情况

## 自定义Hook

通过自定义 Hook，可以对其它Hook的代码进行复用

在企业开发中, 但凡需要抽取代码, 但凡被抽取的代码中用到了其它的Hook,

那么就必须把这些代码抽取到自定义Hook中

注意点: 在React中只有两个地方可以使用Hook

* 函数式组件中

- 自定义Hook中

如何自定义一个Hooks

只要在函数名称前面加上use, 那么就表示这个函数是一个自定义Hook, 就表示可以在这个函数中使用其它的Hook

```javascript
import React, {createContext, useContext} from 'react';

const UserContext = createContext({});
const InfoContext = createContext({});

function App() {
    return (
        <UserContext.Provider value={{name:'lnj', age:18}}>
            <InfoContext.Provider value={{gender:'man'}}>
                <Home/>
                <About/>
            </InfoContext.Provider>
        </UserContext.Provider>
    )
}

function useGetContext() {
    const user = useContext(UserContext);
    const info = useContext(InfoContext);
    return [user, info]
}

function Home() {
    // const user = useContext(UserContext);
    // const info = useContext(InfoContext);
    const [user, info] = useGetContext();
    return (
        <div>
            <p>{user.name}</p>
            <p>{user.age}</p>
            <p>{info.gender}</p>
            <hr/>
        </div>
    )
}
function About() {
    // const user = useContext(UserContext);
    // const info = useContext(InfoContext);
    const [user, info] = useGetContext();
    return (
        <div>
            <p>{user.name}</p>
            <p>{user.age}</p>
            <p>{info.gender}</p>
            <hr/>
        </div>
    )
}
```