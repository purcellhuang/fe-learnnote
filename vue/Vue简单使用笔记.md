## 简介

`Vue` 是一套用于构建用户界面的**渐进式框架**。

与其它大型框架不同的是，`Vue `被设计为可以自底向上逐层应用。

`Vue `的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。

并没有完全遵循`MVVM`模型。

> 严格的` MVVM `要求` View` 不能和 `Model` 直接通信，而 `Vue` 提供了`$refs` 这个属性，让 `Model` 可以直接操作 `View`，违反了这一规定，所以说` Vue `没有完全遵循 `MVVM`。

## 快速上手

```javascript
//引入vue
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>

<div id="app">
  {{ message }}
</div>
 
var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
})
```

## 内置指令

![内置指令.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/0b46ec8b051246858211c4c7ec129fb3~tplv-k3u1fbpfcp-watermark.image)

## 生命周期



## Options

### props

HTML 中的 attribute 名是**大小写不敏感**的，所以浏览器会把所有大写字符解释为小写字符。这意味着当你使用 DOM 中的模板时，`camelCase` (驼峰命名法) 的 prop 名需要使用其等价的 `kebab-case` (短横线分隔命名) 命名：

```
<blog-post post-title="hello!"></blog-post>

props: ['postTitle']
```

注：如果你使用字符串模板，那么这个限制就不存在了。

* 单向数据流

  所有的 prop 都使得其父子 prop 之间形成了一个**单向下行绑定**：父级 prop 的更新会向下流动到子组件中，但是反过来则不行。这样会防止从子组件意外变更父级组件的状态，从而导致你的应用的数据流向难以理解。

* 数据验证

  为了定制 prop 的验证方式，你可以为 `props` 中的值提供一个带有验证需求的对象，而不是一个字符串数组。例如：

  ```javascript
  props: {
      // 基础的类型检查 (`null` 和 `undefined` 会通过任何类型验证)
      propA: Number,
      // 多个可能的类型
      propB: [String, Number],
      // 必填的字符串
      propC: {
        type: String,
        required: true
      },
      // 带有默认值的数字
      propD: {
        type: Number,
        default: 100
      },
      // 带有默认值的对象
      propE: {
        type: Object,
        // 对象或数组默认值必须从一个工厂函数获取
        default: function () {
          return { message: 'hello' }
        }
      },
      // 自定义验证函数
      propF: {
        validator: function (value) {
          // 这个值必须匹配下列字符串中的一个
          return ['success', 'warning', 'danger'].indexOf(value) !== -1
        }
      }
    }
    
  type 可以是下列原生构造函数中的一个：
  String
  Number
  Boolean
  Array
  Object
  Date
  Function
  Symbol
  ```

### data

**一个组件的 `data` 选项必须是一个函数**，因此每个实例可以维护一份被返回对象的独立的拷贝。

### methods

### computed

### watch

### filter

### directives

### components

### mixins

### provide

### inject



## 父子组件通信





## 插槽







