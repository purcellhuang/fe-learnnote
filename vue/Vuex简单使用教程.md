## 简介

`Vuex `是实现组件全局状态（数据）管理的一种机制，可以方便的实现组件之间数据的共享。

**好处：**

* 能够在`vuex`中集中管理共享的数据，易于开发与后期维护
* 能够高效地实现组件之间的数据共享，提高开发效率
* 存储在`vuex`中的数据都是响应式的，能够实时保持数据与页面的同步

## 目录结构

```bash
├── index.html
├── main.js
├── api
│   └── ... # 抽取出API请求
├── components
│   ├── App.vue
│   └── ...
└── store
    ├── index.js          # 我们组装模块并导出 store 的地方
    ├── actions.js        # 根级别的 action
    ├── mutations.js      # 根级别的 mutation
    └── modules
        ├── cart.js       # 购物车模块
        └── products.js   # 产品模块
```

## 基本使用

```javascript
//1.安装依赖
npm i vuex --save

//2.导入依赖
import Vuex from 'vuex'

Vue.use(Vuex)

//3.创建store对象
const store = new Vuex.store({
    state:{
        count: 0
    },
    mutations: {
        add (state) {
            state.count++
        }
    }
})

//store.commit('add')


//4.挂载到vue实例中
new Vue({
    el: '#app',
    render: h => h(app),
    router,
    store
})
```



## 核心概念

### State

`State`提供唯一的公共数据源，所有共享的数据都要统一放到`Store`的`State`中进行存储。

```javascript
//store/index.js
const store = new Vuex.store({
    state:{
        count: 0
    }
})

//home/index.js

//访问state中的数据
//方式一
<p>{{$store.state.count}}</p>

//方式二
<p>{{count}}</p>

import {mapState} from 'vuex'

computed: {
    //将state映射到当前组件的计算属性中
    ...mapState(['count'])
}

```



### Mutation

`Mutation`用于变更`State`中的数据，可以集中监控数据变化。

注意：**mutation 必须是同步函数**

```javascript
//store/index.js

//触发mutation方式一
 mutations: {
     increment (state) {
         state.count++
     }
 }

store.commit('add')

//参数传递
mutations: {
  increment (state, payload) {
    state.count += payload.amount
  }
}

store.commit('increment', {
  amount: 10
})

//触发mutation方式二
import { mapMutations } from 'vuex'

methods: {
    ...mapMutations([
        'increment', // 将 `this.increment()` 映射为 `this.$store.commit('increment')`
        // `mapMutations` 也支持载荷：
        'incrementBy' // 将 `this.incrementBy(amount)` 映射为 `this.$store.commit('incrementBy', amount)`
    ]),
        ...mapMutations({
        add: 'increment' // 将 `this.add()` 映射为 `this.$store.commit('increment')`
    })
}
```

### Action

`Action` 类似于` mutation`，不同在于：

- `Action` 提交的是 `mutation`，而不是直接变更状态。
- `Action `可以包含任意异步操作。

`Action` 函数接受一个与` store` 实例具有相同方法和属性的 `context `对象。

```javascript
const store = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++
    }
  },
  actions: {
    increment (context) {
      context.commit('increment')
    },
    //异步操作
    incrementAsync ({ commit }) {
      setTimeout(() => {
        commit('increment')
      }, 1000)
	}
  }
})

//触发action方式一
store.dispatch('increment')


//触发action方式二
import { mapActions } from 'vuex'

export default {
  // ...
  methods: {
    ...mapActions([
      'increment', // 将 `this.increment()` 映射为 `this.$store.dispatch('increment')`

      // `mapActions` 也支持载荷：
      'incrementBy' // 将 `this.incrementBy(amount)` 映射为 `this.$store.dispatch('incrementBy', amount)`
    ]),
    ...mapActions({
      add: 'increment' // 将 `this.add()` 映射为 `this.$store.dispatch('increment')`
    })
  }
}
```



### Getter

`Getter`用于对`Store`中的数据进行加工处理形成新的数据。

* 类似于`Vue`的计算属性

* `Store`中数据发生变化，`Getter`的数据也会跟着变化

```javascript
getters: {
    showCount: state => {
        return "当前数量：" + state.count
    }
}

store.getters.showCount


/****************************************************/
import { mapGetters } from 'vuex'

export default {
  // ...
  computed: {
  // 使用对象展开运算符将 getter 混入 computed 对象中
    ...mapGetters([
      'doneTodosCount',
      'anotherGetter',
      // ...
    ])
  }
}

...mapGetters({
  // 把 `this.doneCount` 映射为 `this.$store.getters.doneTodosCount`
  doneCount: 'doneTodosCount'
})
```



### Module

由于使用单一状态树，应用的所有状态会集中到一个比较大的对象。当应用变得非常复杂时，`store `对象就有可能变得相当臃肿。

为了解决以上问题，`Vuex `允许我们将 store 分割成**模块（module）**。每个模块拥有自己的 `state`、`mutation`、`action`、`getter`、甚至是嵌套子模块——从上至下进行同样方式的分割：

```javascript
const moduleA = {
  state: () => ({ ... }),
  mutations: { ... },
  actions: { ... },
  getters: { ... }
}

const moduleB = {
  state: () => ({ ... }),
  mutations: { ... },
  actions: { ... }
}

const store = new Vuex.Store({
  modules: {
    a: moduleA,
    b: moduleB
  }
})

store.state.a // -> moduleA 的状态
store.state.b // -> moduleB 的状态

//局部模块
const moduleA = {
  state: () => ({
    count: 0
  }),
  mutations: {
    increment (state) {
      // 这里的 `state` 对象是模块的局部状态
      state.count++
    }
  },

  getters: {
    doubleCount (state, getters, rootState) {
      return state.count * 2
    }
  },
    
  actions:{
      login({ state, commit, rootState }){
          ...
      }
  }
}
```

