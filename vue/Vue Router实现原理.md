## Router本质

根据"不同的hash值"或者"不同的路径地址", 将不同的内容渲染到router-view中

核心：**监听'hash'或'路径'的变化**, 再将不同的内容写到router-view中

## 注册路由

```javascript
//		/router/VueRouter.js
class VueRouter {
  constructor(options) {
  }
}

VueRouter.install = (Vue, options) => {
}

export default VueRouter


//		/router/index.js
import Vue from 'vue'
import VueRouter from './VueRouter'

Vue.use(VueRouter)
const routes = [
]

const router = new VueRouter({
  mode: 'history', // #/home /home
  base: process.env.BASE_URL,
  routes
})

export default router

//		main.js
import Vue from 'vue'
import router from './router/index'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

```

## 提取路由信息

```javascript
//		index.js
import Vue from 'vue'
import VueRouter from './VueRouter'
import Home from '../views/Home.vue'
import About from '../views/About.vue'

Vue.use(VueRouter)
const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  }
]

const router = new VueRouter({
  mode: 'history', // #/home /home
  base: process.env.BASE_URL,
  routes
})

export default router


//		VueRouter.js
class VueRouter {
  constructor(options) {
    this.mode = options.mode || 'hash'
    this.routes = options.routes || []

    this.routesMap = this.createRoutesMap()
    console.log(this.routesMap)
  }

  createRoutesMap () {
    return this.routes.reduce((map, route) => {
      map[route.path] = route.component
      return map
    }, {})
  }
}

// $route  $router
VueRouter.install = (Vue, options) => {

}

export default VueRouter
```

