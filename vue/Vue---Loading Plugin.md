## 编写Loading组件

```vue
<template>
  <div class="loading-container"
       v-show="isShow">
    <div class="loading-box">

    </div>
    <p class="loading-title">{{title}}</p>
  </div>
</template>

<script>
export default {
  name: 'Loading',
  data () {
    return {
      title: '正在加载中...',
      isShow: false
    }
  },
  methods: {
    showLoading () {
      this.isShow = true
    },
    hiddenLoading () {
      this.isShow = false
    }
  }
}
</script>

<style lang="scss" scoped>
.loading-container {
  position: absolute;
  width: 200px;
  height: 200px;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  border-radius: 20px;
  background: rgba(0, 0, 0, 0.5);
  .loading-box {
    box-sizing: border-box;
    border-radius: 50%;
    border: 5px solid #fff;
    border-right-color: red;
    margin: 20px auto;
    width: 100px;
    height: 100px;
    animation: loading 2s linear infinite;
  }
  .loading-title {
    font-size: 16px;
    text-align: center;
    color: red;
  }
}

@keyframes loading {
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
}
</style>

```



## 暴露install方法

```javascript
import Loading from './Loading.vue'

export default {
  install: function (Vue, options) {
    const LoadingConstructor = Vue.extend(Loading)
    const loadingInstance = new LoadingConstructor()
    const div = document.createElement('div')
    document.body.appendChild(div)
    loadingInstance.$mount(div)

    if (options && options.title) {
      loadingInstance.title = options.title
    }
      
    Vue.showLoading = () => {
      oadingInstance.showLoading()
    }
    Vue.hiddenLoading = () => {
      loadingInstance.hiddenLoading()
    }

    Vue.prototype.showLoading = () => {
      loadingInstance.showLoading()
    }
    Vue.prototype.hiddenLoading = () => {
      loadingInstance.hiddenLoading()
    }
  }
}

```



## 安装Loading插件

```javascript
import Vue from 'vue'
import Loading from './plugin/loading/index'

Vue.use(Loading, { title: '加载中...' })

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
```

