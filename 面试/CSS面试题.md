## 盒子模型

```css
 div {
            box-sizing: content-box;
            padding: 10px;
            border: 10px solid #ccc;
            margin: 10px;
            width: 100px;
            height: 100px;
            background-color: red;
        }
```



### 标准模型（默认）

> box-sizing: content-box;

![image-20210727132015252](C:\Users\YMXD\AppData\Roaming\Typora\typora-user-images\image-20210727132015252.png)

### 怪异模型

> box-sizing: border-box;

![image-20210727132108756](C:\Users\YMXD\AppData\Roaming\Typora\typora-user-images\image-20210727132108756.png)

JavaScript中获取元素宽度

* `#div.style.width`：这种方法只有在样式写在行内的时候，才能获取到宽度，并且是带单位的：`100px`。如果不是行内样式，那么获取到的是空的。

* `#div.offsetWidth`：这种方法看盒子模式，如果是 IE 模式的怪异盒子，那么获取到 `100`（`border-box`）；如果是 W3C 的标准盒子（`content-box`），那么获取到 `140`。注意是不带单位的。



## div居中

### 水平居中

* 行内元素

  ```
  //父级元素
  text-align: center;
  ```

* 块级元素

  ```
  //1.定宽  width:300px
  margin：0 50px;
  //2.不定宽
  margin：0 auto;
  ```

* flex布局

  ```
    display:flex;
    justify-content:center;
  ```

* 绝对定位

  ```
   //1.已知宽度，绝对定位的居中 ，上下左右都为0，margin:auto
   position: absolute;
   left: 0;
   top: 0;
   right: 0;
   bottom: 0;
   margin: auto;
   
   //2.
   position: absolute;
   top: 0;
   left: 50%;
   transform: translateX(-50%);
  ```

### 垂直居中

* 行内元素

  ```
  //文字、图片
  line-height：height；
  ```

* 块级元素

  ```
  //display: flex
  display: flex;
  align-items: center;
  justify-content: center;  /*水平居中*/
  
  //绝对定位 + margin: auto;
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  
  //绝对定位 + transform
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  ```

  

## 块级格式化上下文

### BFC创建方法

* 根元素（html）
* 浮动元素（float不等于none）
* 绝对定位（position等于absolute或fixed）
* 行内块状元素（inline-block）
* overflow != visible
* flex布局
* 表格单元格（table-cell）

最常见的就是`overflow:hidden`、`float:left/right`、`position:absolute`。

### BFC作用

* 内部的盒会在垂直方向一个接一个排列（可以看作BFC中有一个的常规流）；

* 处于同一个BFC中的元素相互影响，可能会发生margin collapse；

* 每个元素的margin box的左边，与容器块border box的左边相接触(对于**从左往右的格式化**，否则相反)。即使存在浮动也是如此；

* BFC就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素，反之亦然；

* 计算BFC的高度时，考虑BFC所包含的所有元素，连浮动元素也参与计算；

* 浮动盒区域不叠加到BFC上；

解决浮动高度坍塌、margin合并、防止与浮动元素重叠...

## 层叠上下文

