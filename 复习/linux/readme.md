## 文件传输工具

> yum install -y lrzsz	
>
> yum install -y unzip zip	//解析工具（zip）
>
> 打包
>
> tar -zcvf 

### zip

```
压缩文件：zip  压缩文件名.zip  源文件　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
压缩目录：zip  -r  压缩文件名.zip  源目录
解压缩： unzip  压缩文件.zip
```

### rar

```
解压文件1: unrar  x  压缩文件.rar (在一个文件夹下，更好)
解压文件2: unrar  e  压缩文件.rar (分散在当前文件夹下)
```

### tar

```
打包文件: tar  -zcvf  打包文件名.tar  源文件 
解打包：  tar  -zxvf  源文件.tar

参数：
-c: 打包
-x: 解打包
-v: 显示过程
-f: 指定打包后的文件名此命令只负责打包文件，并不进行压缩。
```