[TOC]

### Cache Manifest

离线缓存是HTML5新引入的技术，能够让你的Web应用程序指定哪些文件可以缓存在本地，使得你的网络断开时依然可以通过本地的缓存来进行访问浏览。

#### 使用

html新增了一个manifest属性，可以用来指定当前页面的manifest文件。创建一个和html同名的manifest文件，比如页面为index.html，那么可以建一个index.manifest的文件，然后给index.html的html标签添加如下属性即可：

 ```xml
 <html lang="en" manifest="index.manifest">
 ```

注意：manifest 文件需要配置正确的 MIME-type，即 "text/cache-manifest"。必须在 web 服务器上进行配置。

例如在tomcat服务器上的web.xml需要添加一下信息。

```xml
<mime-mapping>  
  <extension>manifest</extension>  
  <mime-type>text/cache-manifest</mime-type>  
</mime-mapping> 
```

**扩展名".appcache"为后缀，不需要我们再进行服务器端的配置了。就可以纯前端的进行离线缓存的操作。**

#### Manifest文件

manifest 文件是简单的文本文件，它告知浏览器被缓存的内容（以及不缓存的内容）。

manifest 文件可分为三个部分：

- *CACHE* - 在此标题下列出的文件将在首次下载后进行缓存

  标识出哪些文件需要缓存，可以是相对路径也可以是绝对路径。

- *NETWORK* - 在此标题下列出的文件需要与服务器的连接，且不会被缓存

  这一部分是要绕过缓存直接读取的文件，可以使用通配符＊。

- *FALLBACK* - 在此标题下列出的文件规定当页面无法访问时的回退页面（比如 404 页面）

  指定了一个后备页面，当资源无法访问时，浏览器会使用该页面。该段落的每条记录都列出两个 URI

  两个 URI 都必须使用相对路径并且与清单文件同源。可以使用通配符。

其中NETWORK和FALLBACK为可选项。 **而第一行CACHE MANIFEST为固定格式，必须写在前面。** 以#号开头的是注释，一般会在第二行写个版本号，用来在缓存的文件更新时，更改manifest的作用，可以是版本号，时间戳或者md5码等等。

```manifest
CACHE MANIFEST
# 2021-08-24 v1.0.0

CACHE:
/theme.css
/logo.gif
/main.js

NETWORK:
login.php

# 注意: 第一个 URI 是资源，第二个是替补。
FALLBACK:
/html/ /offline.html
```

#### 更新缓存

* **更新manifest文件**

  给manifest添加或删除文件，都可更新缓存，如果我们更改了js，而没有新增或删除，前面例子中注释中的版本号、时间戳或者md5码等进行修改，都可以很好的用来更新manifest文件

* **通过javascript操作**

  html5中引入了js操作离线缓存的方法，下面的js可以手动更新本地缓存。

  ```javascript
  window.applicationCache.update();
  ```

* **清除浏览器缓存**

  如果用户清除了浏览器缓存（手动或用其他一些工具）都会重新下载文件。

**监听本地缓存更新**

```javascript
applicationCache.addEventListener("updateready",function(){
    applicationCache.swapCache();      // 手工更新本地缓存
    location.reload();    //重新加载页面页面
},true);
```

#### 注意

* 浏览器对缓存数据的容量限制可能不太一样（某些浏览器设置的限制是每个站点 5MB）。
* 如果manifest文件，或者内部列举的某一个文件不能正常下载，整个更新过程都将失败，浏览器继续全部使用老的缓存。
* 引用manifest的html必须与manifest文件同源，在同一个域下。
* FALLBACK中的资源必须和manifest文件同源。
* 当一个资源被缓存后，该浏览器直接请求这个绝对路径也会访问缓存中的资源。
* 站点中的其他页面即使没有设置manifest属性，请求的资源如果在缓存中也从缓存中访问。
* 当manifest文件发生改变时，资源请求本身也会触发更新。



###  Service Workers

Service workers 本质上充当 Web 应用程序、浏览器与网络（可用时）之间的代理服务器。这个 API 旨在创建有效的离线体验，它会拦截网络请求并根据网络是否可用采取来适当的动作、更新来自服务器的的资源。它还提供入口以推送通知和访问后台同步 API。

Service worker是一个注册在指定源和路径下的事件驱动[worker](https://developer.mozilla.org/zh-CN/docs/Web/API/Worker)。它采用JavaScript控制关联的页面或者网站，拦截并修改访问和资源请求，细粒度地缓存资源。你可以完全控制应用在特定情形（最常见的情形是网络不可用）下的表现。

Service worker运行在worker上下文，因此它不能访问DOM。相对于驱动应用的主JavaScript线程，它运行在其他线程中，所以不会造成阻塞。它设计为完全异步，同步API（如[XHR](https://developer.mozilla.org/zh-CN/docs/Web/API/XMLHttpRequest)和[localStorage (en-US)](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API)）不能在service worker中使用。

#### 使用场景

Service workers也可以用来做这些事情：

- 后台数据同步
- 响应来自其它源的资源请求
- 集中接收计算成本高的数据更新，比如地理位置和陀螺仪信息，这样多个页面就可以利用同一组数据
- 在客户端进行CoffeeScript，LESS，CJS/AMD等模块编译和依赖管理（用于开发目的）
- 后台服务钩子
- 自定义模板用于特定URL模式
- 性能增强，比如预取用户可能需要的资源，比如相册中的后面数张图片

未来service workers能够用来做更多使web平台接近原生应用的事。 值得关注的是，其他标准也能并且将会使用service worker，例如:

- [后台同步](https://github.com/slightlyoff/BackgroundSync)：启动一个service worker即使没有用户访问特定站点，也可以更新缓存
- [响应推送](https://developer.mozilla.org/zh-CN/docs/Web/API/Push_API)：启动一个service worker向用户发送一条信息通知新的内容可用
- 对时间或日期作出响应
- 进入地理围栏

#### 生命周期

此时，你的服务工作者(service worker)将遵守以下生命周期：

1. 下载
2. 安装
3. 激活

用户首次访问service worker控制的网站或页面时，service worker会立刻被下载。

之后，在以下情况将会触发更新:

- 一个前往作用域内页面的导航
- 在 service worker 上的一个事件被触发并且过去 24 小时没有被下载

无论它与现有service worker不同（字节对比），还是第一次在页面或网站遇到service worker，如果下载的文件是新的，安装就会尝试进行。

如果这是首次启用service worker，页面会首先尝试安装，安装成功后它会被激活。

如果现有service worker已启用，新版本会在后台安装，但不会被激活，这个时序称为worker in waiting。直到所有已加载的页面不再使用旧的service worker才会激活新的service worker。只要页面不再依赖旧的service worker，新的service worker会被激活（成为active worker）。

你可以监听[`InstallEvent`](https://developer.mozilla.org/zh-CN/docs/Web/API/InstallEvent)，事件触发时的标准行为是准备service worker用于使用，例如使用内建的storage API来创建缓存，并且放置应用离线时所需资源。

还有一个activate事件，触发时可以清理旧缓存和旧的service worker关联的东西。

Servcie worker可以通过 [`FetchEvent`](https://developer.mozilla.org/zh-CN/docs/Web/API/FetchEvent) 事件去响应请求。通过使用 [`FetchEvent.respondWith`](https://developer.mozilla.org/zh-CN/docs/Web/API/FetchEvent/respondWith) 方法，你可以任意修改对于这些请求的响应。

**注意**: 因为`oninstall`和`onactivate`完成前需要一些时间，service worker标准提供一个`waitUntil`方法，当`oninstall`或者`onactivate`触发时被调用，接受一个promise。在这个 promise被成功resolve以前，功能性事件不会分发到service worker。

#### 固定套路使用

* 注册Service Worker

  ```javascript
  if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/sw-demo-cache.js', { scope: '/' });
  }
  ```

  `register()` 方法的一个重要细节是 Service Worker 文件的位置。在本例中，可以看到 Service Worker 文件位于域的根目录，这意味着 Service Worker 范围将是这个域下的

  `scope` 参数是选填的，可以被用来指定你想让 service worker 控制的内容的子目录。

* `sw-demo-cache.js`

  ```javascript
  var VERSION = 'v3';
  
  //install 事件会在注册完成之后触发。install 事件一般是被用来填充你的浏览器的离线缓存能力
  // 缓存
  self.addEventListener('install', function(event) {
    event.waitUntil(
      caches.open(VERSION).then(function(cache) {
        return cache.addAll([
          './start.html',
          './static/jquery.min.js',
          './static/mm1.jpg'
        ]);
      })
    );
  });
  
  // 缓存更新
  self.addEventListener('activate', function(event) {
    event.waitUntil(
      caches.keys().then(function(cacheNames) {
        return Promise.all(
          cacheNames.map(function(cacheName) {
            // 如果当前版本和缓存版本不一致
            if (cacheName !== VERSION) {
              return caches.delete(cacheName);
            }
          })
        );
      })
    );
  });
  
  //给 service worker 添加一个 fetch 的事件监听器，接着调用 event 上的 respondWith() 方法来劫持我们的 HTTP 响应
  // 捕获请求并返回缓存数据
  //caches.match(event.request) 允许我们对网络请求的资源和 cache 里可获取的资源进行匹配，查看是否缓存中有相应的资源。这个匹配通过 url 和 vary header进行，就像正常的 http 请求一样。
  self.addEventListener('fetch', function(event) {
    event.respondWith(
      caches.match(event.request).then(function() {
        return fetch(event.request).then(function(response) {
          return caches.open('VERSION').then(function(cache) {
            cache.put(event.request, response.clone());
            return response;
          });
        });
      }).catch(function() {
        return caches.match('/sw-test/gallery/myLittleVader.jpg');
      })
    );
  });
  ```

#### 要求HTTPS的原因

  在构建 Web 应用程序时，通过 localhost 使用 Service Workers，但是一旦将其部署到生产环境中，就需要准备好 HTTPS( 这是使用HTTPS 的最后一个原因)。

  使用 Service Worker，可以很**容易被劫持连接并伪造响应**。如果不使用 HTTPs，人的web应用程序就容易受到黑客的攻击。

  为了更安全，你需要在通过 HTTPS 提供的页面上注册 Service Worker，以便知道浏览器接收的 Service Worker 在通过网络传输时未被修改。