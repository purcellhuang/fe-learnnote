## Ajax简介

AJAX的全程是Asynchronous JAVAScript and XML（异步的javaScript和xml）;
ajax不是新的编程语言，而是一种使用标准的新方法。ajax是与服务器交换数据并更新部分网页的艺术；

## 创建 XMLHttpRequest 对象

```javascript
let xhr = null
if(window.XMLHttpRequest){
    xhr = new XMLHttpRequest()
}else{
    xhr=new ActiveXObject("Microsoft.XMLHTTP");
}
```

##  向服务器发送请求

```javascript
let xhr = null
if(window.XMLHttpRequest){
    xhr = new XMLHttpRequest()
}else{
    xhr=new ActiveXObject("Microsoft.XMLHTTP");
}

// GET
// open(method,url,async)：规定请求的类型、URL 以及是否异步处理请求
// method：请求的类型；GET 或 POST
// url：文件在服务器上的位置
// async：true（异步）或 false（同步）
xhr.open('GET','/index.html',true)
xhr.send()


// POST
// 如果需要像 HTML 表单那样 POST 数据，请使用 setRequestHeader() 来添加 HTTP 头。然后在 send() 方法中规定您希望发送的数据：
xhr.open('POST','/index.html',true)
xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
xmlhttp.send("fname=Henry&lname=Ford");
```

## onreadystatechange 事件

如需获得来自服务器的响应，请使用 XMLHttpRequest 对象的 responseText 或 responseXML 属性。

| 属性               | 描述                                                         |
| :----------------- | :----------------------------------------------------------- |
| onreadystatechange | 存储函数（或函数名），每当 readyState 属性改变时，就会调用该函数。 |
| readyState         | 存有 XMLHttpRequest 的状态。从 0 到 4 发生变化。0: 请求未初始化1: 服务器连接已建立2: 请求已接收3: 请求处理中4: 请求已完成，且响应已就绪 |
| status             | 200: "OK" 404: 未找到页面                                    |

```javascript
xhr.onreadystatechange=function(){
    if (xmlhttp.readyState==4 && xmlhttp.status==200){
        document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
    }
}
```

