### 变量和类型

- 1.`JavaScript`规定了几种语言类型

  * null
  * undefined
  * number
  * string
  * boolean
  * symbol
  * bigint
  * object

- 2.`JavaScript`对象的底层数据结构是什么

- 3.`Symbol`类型在实际开发中的应用、可手动实现一个简单的`Symbol`

  防止变量命名污染。

- 4.`JavaScript`中的变量在内存中的具体存储形式

  * 基本类型

    栈

  * 引用类型

    堆

- 5.基本类型对应的内置对象，以及他们之间的装箱拆箱操作

- 6.理解值类型和引用类型

  基本类型存放的是值，复杂类型存放的是引用。

- 7.`null`和`undefined`的区别

  null代表空值。undefined代表已声明，但未定义值。变量默认值。函数默认返回值。

  undefined不是关键字，可以被修改。

  一般使用 undefined === void 0   判断undefined

- 8.至少可以说出三种判断`JavaScript`数据类型的方式，以及他们的优缺点，如何准确的判断数组类型

  * typeof

    `typeof` 对于基本类型，除了 `null` 都可以显示正确的类型

    `typeof` 对于对象，除了函数都会显示 `object`

  * constructor

  * instanceof

  * toString

    ```javascript
      Object.prototype.toString.call(num), // [object Number]
      Object.prototype.toString.call(str), // [object String]
      Object.prototype.toString.call(bool), // [object Boolean]
      Object.prototype.toString.call(arr), // [object Array]
      Object.prototype.toString.call(json), // [object Object]
      Object.prototype.toString.call(func), // [object Function]
      Object.prototype.toString.call(und), // [object Undefined]
      Object.prototype.toString.call(nul), // [object Null]
      Object.prototype.toString.call(date), // [object Date]
      Object.prototype.toString.call(reg), // [object RegExp]
      Object.prototype.toString.call(error), // [object Error]
    ```

    

  **数组判断方法：**

  ```javascript
  Array.isArray(arr)
  
  arr isntanceof Array
  
  arr.constructor === Array
  
  Object.prototype.toString.call(arr) === '[object Array]'
  ```

  

- 9.可能发生隐式类型转换的场景以及转换原则，应如何避免或巧妙应用

  * boolean

    除了 `undefined`， `null`， `false`， `NaN`， `''`， `0`， `-0`，其他所有值都转为 `true`，包括所有对象。

    隐式转换  !!

  * string

    * .toString()

      不可以转null和undefined

    * String()

    * number + ''

  * number

    * Number()
    * parseInt()
      * 结果：如果第一个字符是数字会解析知道遇到非数字结束，如果第一个字符不是数字或者符号就返回NaN
    * parseFloat

- 10.出现小数精度丢失的原因，`JavaScript`可以存储的最大数字、最大安全数字，`JavaScript`处理大数字的方法、避免精度丢失的方法

### 原型和原型链

- 1.理解原型设计模式以及`JavaScript`中的原型规则

  ![img](assets/JavaScript基础/68747470733a2f2f79636b2d313235343236333432322e636f732e61702d7368616e676861692e6d7971636c6f75642e636f6d2f626c6f672f323031392d30362d30312d3033333932352e706e67)

- 2.`instanceof`的底层实现原理，手动实现一个`instanceof`

  ```javascript
  function instanceof(obj, clazz) {
      let proto = obj.__proto__;
      while(proto){
          if(proto === clazz.prototype){
              return true;
          }
          proto = proto.__proto__;
      }
      return false;
  }
  ```

  

- 4.实现继承的几种方式以及他们的优缺点

  * ES5

    ```
    
    ```

    

  * ES6

    ```
    
    ```

    

- 5.至少说出一种开源项目(如`Node`)中应用原型继承的案例

- 6.可以描述`new`一个对象的详细过程，手动实现一个`new`操作符

  ```javascript
  function New(clazz, ...args){
      const obj = Object.create(null);
      obj.__proto__ = clazz.prototype;
      
      //const obj = Object.create(clazz.prototype);
      
      const result = clazz.call(obj, ...args);
      if((typeof result === object && result !== null) || typeof result === function){
          return result;
      }
      return obj;
  }
  ```

  

- 7.理解`es6 class`构造以及继承的底层实现原理

  ```
  
  ```

  

### 作用域和闭包

- 1.理解词法作用域和动态作用域

  词法作用域是在定义时确定的，而动态作用域是在运行时确定的。

  JavaScript使用的是词法作用域。

  * 词法作用域

    词法作用域就是定义在词法阶段的作用域，是由写代码时将变量和块作用域写在哪里来决定的，因此当词法分析器处理代码时会保持作用域不变

  * 动态作用域

    动态作用域并不关心函数和作用域是如何声明以及在任何处声明的，只关心它们从何处调用。换句话说，作用域链是基于调用栈的，而不是代码中的作用域嵌套

- 2.理解`JavaScript`的作用域和作用域链

- 3.理解`JavaScript`的执行上下文栈，可以应用堆栈信息快速定位问题

- 4.`this`的原理以及几种不同使用场景的取值

  * 默认绑定

    默认绑定我们可以理解为函数调用时无任何调用前缀的情景，this指向全局对象（非严格模式）

  * 隐式绑定

    如果函数调用时，前面存在调用它的对象，那么this就会隐式绑定到这个对象上。

    如果函数调用前存在多个对象，this指向距离调用自己最近的对象

  * 显式绑定

    通过call、apply以及bind方法改变this的行为

    指向参数提供的是null或者undefined，那么 this 将指向全局对象

  * new绑定

    以构造器的prototype属性为原型，创建新对象；

    将this(可以理解为上句创建的新对象)和调用参数传给构造器，执行；

    如果构造器没有手动返回对象，则返回第一步创建的对象

  * 箭头函数绑定

    箭头函数的this指向取决于外层作用域中的this，外层作用域或函数的this指向谁，箭头函数中的this便指向谁

- 5.闭包的实现原理和作用，可以列举几个开发中闭包的实际应用

  内部函数持有外部函数的变量，该函数与变量的集合称为闭包。

  **应用：**

  * 实现私有变量
  * 屏蔽内部变量
  * 延迟内部变量生命周期

- 6.理解堆栈溢出和内存泄漏的原理，如何防止

  V8引擎由两个主要部件组成:

  - emory Heap(内存堆) — 内存分配地址的地方
  - Call Stack(调用堆栈) — 代码执行的地方

  **堆栈溢出：**

  JavaScript是一种单线程编程语言，这意味着它只有一个调用堆栈。因此，它一次只能做一件事。

  调用栈是一种数据结构，它记录了我们在程序中的位置。如果我们运行到一个函数，它就会将其放置到栈顶，当从这个函数返回的时候，就会将这个函数从栈顶弹出，这就是调用栈做的事情。

  "堆栈溢出"，当你达到调用栈最大的大小的时候就会发生这种情况，而且这相当容易发生，特别是在你写递归的时候却没有全方位的测试它。

  **内存泄漏：**

  内存泄漏可以定义为:不再被应用程序所需要的内存,出于某种原因,它不会返回到操作系统或空闲内存池中。

  * 全局变量

    ```javascript
    function foo(arg) {
        //window.bar
        bar = "this is a hidden global variable";
    }
    
    function foo2(){
        //this-->window
        this.varable = "hello"
    }
    foo2();//调用后，this指向window全局变量
    ```

    

  * 定时器

    定时器不清除，一直占用内存，得不到释放，或者在定时器内调用外部函数，得不到释放；

  * 闭包

    内部函数引用外部函数变量，得不到释放，比如：

  * 脱离DOM的引用

    ```javascript
    const refA = document.getElementById('refA');
    document.body.removeChild(refA); // dom删除了
    console.log(refA, 'refA'); // 但是还存在引用能console出整个div 没有被回收
    refA = null;
    console.log(refA, 'refA'); // 解除引用
    ```

- 7.如何处理循环的异步操作

  ```javascript
  async function asyncLoop() {
      for (let i = 0; i < 10; i++) {
          //同步等待
          await doSomething()
      }
  }
  
  //递归
  function doSomething(i, arr = [1,2,3,4]){
      work().then(()=>{
          if(i < arr.length){
              i++
              doSomething(i)
          }
      })
  }
  ```

  

- 8.理解模块化解决的实际问题，可列举几个模块化方案并理解其中原理

### 执行机制

- 1.为何`try`里面放`return`，`finally`还会执行，理解其内部机制

  

- 2.`JavaScript`如何实现异步编程，可以详细描述`EventLoop`机制

  ![img](assets/JavaScript基础/2166812-20201005183955978-649108979.png)

  1. js引擎将所有代码放入执行栈，并依次弹出并执行，这些任务有的是同步有的是异步(宏任务或微任务)。
  2. 如果在执行 栈中代码时发现宏任务则交个浏览器相应的线程去处理，浏览器线程在正确的时机(比如定时器最短延迟时间)将宏任务的消息(或称之为回调函数)推入宏任务队列。而宏任务队列中的任务只有执行栈为空时才会执行。
  3. 如果执行 栈中的代码时发现微任务则推入微任务队列，和宏任务队列一样，微任务队列的任务也在执行栈为空时才会执行，但是微任务始终比宏任务先执行。
  4. 当执行栈为空时，eventLoop转到微任务队列处，依次弹出首个任务放入执行栈并执行，如果在执行的过程中又有微任务产生则推入队列末尾，这样循环直到微任务队列为空。
  5. 当执行栈和微任务队列都为空时，eventLoop转到宏任务队列，并取出队首的任务放入执行栈执行。需要注意的是宏任务每次循环只执行一个。
  6. 重复1-5过程
  7. ...直到栈和队列都为空时，代码执行结束。引擎休眠等待直至下次任务出现。

  **注意：**

  1. 宏任务每次只取一个，执行之后马上执行微任务。

  2. 微任务会依次执行，直到微任务队列为空。

  3. 图中没有画UI rendering的节点，因为这个是由浏览器自行判断决定的，但是只要执行UI rendering，它的节点是在执行完所有的microtask之后，下一个macrotask之前，紧跟着执行UI render。

- 3.宏任务和微任务分别有哪些

  * 宏任务

    setTimeout、setInterval、requestAnimationFrame 、I/O、UI rendering

  * 微任务

    Promise、Object.observe、MutationObserver、

- 4.可以快速分析一个复杂的异步嵌套逻辑，并掌握分析方法

- 5.使用`Promise`实现串行

- 6.`Node`与浏览器`EventLoop`的差异

  > 它是一个在 JavaScript 引擎等待任务，执行任务和进入休眠状态等待更多任务这几个状态之间转换的无限循环。 我们都知道JavaScript引擎是单线程的，至于为什么是单线程主要是出于JavaScript的使用场景考虑，作为浏览器的脚本语言，js的主要任务是主要是实现用户与浏览器的交互，以及操作dom，如果设计成多线程会增加复杂的同步问题。想象一个场景：多个线程同时操作dom，浏览器渲染引擎该使用哪个线程的结果。当然为了利用多核CPU的计算能力，HTML5提出Web Worker标准，允许JavaScript脚本创建多个线程，但是子线程完全受主线程控制，且不得操作DOM。所以，这个新标准并没有改变JavaScript单线程的本质。
  >
  > 虽然JS是单线程的，但浏览器却是多线程，其中几个典型的线程已经在图中表示出来，而**eventLoop就是沟通JS引擎线程和浏览器线程的桥梁，也是浏览器实现异步非阻塞模型的关键**。

  * 浏览器EventLoop

    

  * Node EventLoop

- 7.如何在保证页面运行流畅的情况下处理海量数据

  海量数据（dom数量、dom渲染）

  * 分页
* 时间切片（requestAnimationFrame）
  
* 虚拟列表
  
  一般使用 **分页 + 虚拟列表**

### 语法和API

- 1.理解`ECMAScript`和`JavaScript`的关系

  ECMAScript和JavaScript的关系是，前者是后者的规格，后者是前者的一种实现

- 2.熟练运用`es5`、`es6`提供的语法规范，

- 3.熟练掌握`JavaScript`提供的全局对象（例如`Date`、`Math`）、全局函数（例如`decodeURI`、`isNaN`）、全局属性（例如`Infinity`、`undefined`）

- 4.熟练应用`map`、`reduce`、`filter` 等高阶函数解决问题

- 5.`setInterval`需要注意的点，使用`settimeout`实现`setInterval`

  

- 6.`JavaScript`提供的正则表达式`API`、可以使用正则表达式（邮箱校验、`URL`解析、去重等）解决常见问题

- 7.`JavaScript`异常处理的方式，统一的异常处理方案

  ```
  try {
  
  }catch(error) {
  
  }
  ```

  
