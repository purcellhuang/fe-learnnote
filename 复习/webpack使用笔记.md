## webpack安装

```
# 安装
npm init -y
npm install --save-dev webpack webpack-cli

# 打包
npx webpack index.js
注意点:
index.js就是需要打包的文件
打包之后的文件会放到dist目录中, 名称叫做main.js

我们在打包JS文件的时候需要输入:  npx webpack index.js
这句指令的含义是: 利用webpack将index.js和它依赖的模块打包到一个文件中 
其实在webpack指令中除了可以通过命令行的方式告诉webpack需要打包哪个文件以外,
还可以通过配置文件的方式告诉webpack需要打包哪个文件

配置文件的名称必须叫做: webpack.config.js, 否则直接输入 npx webpack打包会出错
如果要使用其它名称, 那么在输入打包命令时候必须通过 --config 指定配置文件名称
npx webpack --config xxx
```



## webpack配置文件

```javascript
// webpack.config.js

/*------------------------------------------------------------*/
const path = require('path')
module.exports = {
  /*
  mode: 指定打包的模式, 模式有两种
  一种是开发模式(development): 不会对打包的JS代码进行压缩
  还有一种就是上线(生产)模式(production): 会对打包的JS代码进行压缩
  * */
  mode: 'development', // 'production' | 'development'
  /*
  打包后代码和打包之前代码的映射关系
  eval:
    不会单独生成sourcemap文件, 会将映射关系存储到打包的文件中, 并且通过eval存储
    优势: 性能最好
    缺点: 业务逻辑比较复杂时候提示信息可能不全面不正确

  source-map:
    会单独生成sourcemap文件, 通过单独文件来存储映射关系
    优势: 提示信息全面,可以直接定位到错误代码的行和列
    缺点: 打包速度慢

  inline:
    不会单独生成sourcemap文件, 会将映射关系存储到打包的文件中, 并且通过base64字符串形式存储

  cheap:
    生成的映射信息只能定位到错误行不能定位到错误列

  module:
    不仅希望存储我们代码的映射关系, 还希望存储第三方模块映射关系, 以便于第三方模块出错时也能更好的排错

  企业开发配置:
  development: eval-cheap-module-source-map
  只需要行错误信息, 并且包含第三方模块错误信息, 并且不会生成单独sourcemap文件

  production: cheap-module-source-map
  只需要行错误信息, 并且包含第三方模块错误信息, 并且会生成单独sourcemap文件

  * */
  devtool: 'eval-cheap-module-source-map',
  /*
  entry: 指定需要打包的文件
  * */
  entry: './index.js',
  /*
  output: 指定打包之后的文件输出的路径和输出的文件名称
  * */
  output: {
    /*
    filename: 指定打包之后的JS文件的名称
    * */
    filename: 'bundle.js',
    /*
    path: 指定打包之后的文件存储到什么地方
    * */
    path: path.resolve(__dirname, 'bundle')
  }
}
```

## Loaders

webapck的本质是一个模块打包工具, 所以webpack默认只能处理JS文件,不能处理其他文件,因为其他文件中没有模块的概念, 但是在企业开发中我们除了需要对JS进行打包以外,还有可能需要对图片/CSS等进行打包, 所以为了能够让webpack能够对其它的文件类型进行打包,在打包之前就必须将其它类型文件转换为webpack能够识别处理的模块,用于将其它类型文件转换为webpack能够识别处理模块的工具我们就称之为loader

### file-loader

```javascript
// https://www.webpackjs.com/loaders/file-loader/
// npm install --save-dev file-loader
module.exports = {
    module: {
        rules: [
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            // 指定打包后文件名称
                            name: '[name].[ext]',
                            // 指定打包后文件存放目录
                            outputPath: 'images/',
                            // 指定托管服务器地址(统一替换图片地址)
                            publicPath: 'http://www.it666.com/images/'
                        }
                    }
                ]
            }
        ]
    }
}
```

#### 打包字体图标

```javascript
// 字体图标中也用到了url用到了文件, 所以我们需要通过file-loader来处理字体图标文件
{
    test: /\.(eot|svg|ttf|woff|woff2)$/,
    use:[{
        loader: "file-loader",
        options: {
            name: "[name].[ext]",
            outputPath: "font/",
        }
    }]
}
```



### url-loader

```javascript
// url-loader 功能类似于 file-loader，
// 但是在文件大小（单位 byte）低于指定的限制时，可以返回一个 DataURL

// https://www.webpackjs.com/loaders/url-loader/

// npm install --save-dev url-loader

// 图片比较小的时候直接转换成base64字符串图片, 减少请求次数
// 图片比较大的时候由于生成的base64字符串图片也比较大, 就保持原有的图片

module.exports = {
    /*
    配置sourcemap
    development: cheap-module-eval-source-map
    production: cheap-module-source-map
    * */
    devtool: "eval-cheap-module-source-map",
    /*
    mode: 指定打包的模式, 模式有两种
    一种是开发模式(development): 不会对打包的JS代码进行压缩
    还有一种就是上线(生产)模式(production): 会对打包的JS代码进行压缩
    * */
    mode: "development", // "production" | "development"
    /*
    entry: 指定需要打包的文件
    * */
    entry: "./index.js",
    /*
    output: 指定打包之后的文件输出的路径和输出的文件名称
    * */
    output: {
        /*
        filename: 指定打包之后的JS文件的名称
        * */
        filename: "bundle.js",
        /*
        path: 指定打包之后的文件存储到什么地方
        * */
        path: path.resolve(__dirname, "bundle")
    },
    /*
    module: 告诉webpack如何处理webpack不能够识别的文件
    * */
    module: {
        rules: [
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            /*
                            limit: 指定图片限制的大小
                            如果被打包的图片超过了限制的大小, 就会将图片保存为一个文件
                            如果被打包的图片没有超过限制的大小, 就会将图片转换成base64的字符串
                            注意点:
                            对于比较小的图片, 我们将图片转换成base64的字符串之后, 可以提升网页的性能(因为减少了请求的次数)
                            对于比较大的图片, 哪怕我们将图片转换成了base64的字符串之后, 也不会提升网页的性能, 还有可能降低网页的性能
                            (因为图片如果比较大, 那么转换之后的字符串也会比较多, 那么网页的体积就会表达, 那么访问的速度就会变慢)
                            * */
                            limit: 1024 * 100,
                            // 指定打包后文件名称
                            name: '[name].[ext]',
                            // 指定打包后文件存放目录
                            outputPath: 'images/',
                        }
                    }
                ]
            }
        ]
    }
};
```



### css-loader

和图片一样webpack默认能不能处理CSS文件, 所以也需要借助loader将CSS文件转换为webpack能够处理的类型

```javascript
// npm install --save-dev css-loader
// npm install style-loader --save-dev

// css-loader:   解析css文件中的@import依赖关系
// style-loader: 将webpack处理之后的内容插入到HTML的HEAD代码中

const path = require("path");

module.exports = {
    /*
    配置sourcemap
    development: cheap-module-eval-source-map
    production: cheap-module-source-map
    * */
    devtool: "cheap-module-eval-source-map",
    /*
    mode: 指定打包的模式, 模式有两种
    一种是开发模式(development): 不会对打包的JS代码进行压缩
    还有一种就是上线(生产)模式(production): 会对打包的JS代码进行压缩
    * */
    mode: "development", // "production" | "development"
    /*
    entry: 指定需要打包的文件
    * */
    entry: "./index.js",
    /*
    output: 指定打包之后的文件输出的路径和输出的文件名称
    * */
    output: {
        /*
        filename: 指定打包之后的JS文件的名称
        * */
        filename: "bundle.js",
        /*
        path: 指定打包之后的文件存储到什么地方
        * */
        path: path.resolve(__dirname, "bundle")
    },
    /*
    module: 告诉webpack如何处理webpack不能够识别的文件
    * */
    module: {
        rules: [
            // 打包图片规则
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // 指定图片限制的大小
                            limit: 1024 * 100,
                            // 指定打包后文件名称
                            name: '[name].[ext]',
                            // 指定打包后文件存放目录
                            outputPath: 'images/',
                        }
                    }
                ]
            },
            // 打包CSS规则
            {
                test: /\.css$/,
                /*
                css-loader:   解析css文件中的@import依赖关系
                style-loader: 将webpack处理之后的内容插入到HTML的HEAD代码中
                * */
                // use: [ 'style-loader', 'css-loader' ]
                use:[
                    {
                        loader: "style-loader",
                    },
                    {
                        loader: "css-loader"
                    }
                ]
            }
        ]
    }
};
```

#### css模块化

默认情况下通过import "./xxx.css"导入的样式是全局样式

也就是只要被导入, 在其它文件中也可以使用

如果想要导入的CSS文件只在导入的文件中有效, 那么就需要开启CSS模块化

```
{
    loader: "css-loader",
    options: {
        modules: true // 开启CSS模块化
    }
}
```

然后在导入的地方通过 import xxx from "./xxx.css"导入

然后在使用的地方通过 xxx.className方式使用即可

```

```



### less-loader

自动将less转换为CSS

```javascript
// npm install --save-dev less

// npm install --save-dev less-loader

const path = require("path");

module.exports = {
    /*
    配置sourcemap
    development: cheap-module-eval-source-map
    production: cheap-module-source-map
    * */
    devtool: "cheap-module-eval-source-map",
    /*
    mode: 指定打包的模式, 模式有两种
    一种是开发模式(development): 不会对打包的JS代码进行压缩
    还有一种就是上线(生产)模式(production): 会对打包的JS代码进行压缩
    * */
    mode: "development", // "production" | "development"
    /*
    entry: 指定需要打包的文件
    * */
    entry: "./index.js",
    /*
    output: 指定打包之后的文件输出的路径和输出的文件名称
    * */
    output: {
        /*
        filename: 指定打包之后的JS文件的名称
        * */
        filename: "bundle.js",
        /*
        path: 指定打包之后的文件存储到什么地方
        * */
        path: path.resolve(__dirname, "bundle")
    },
    /*
    module: 告诉webpack如何处理webpack不能够识别的文件
    * */
    module: {
        rules: [
            // 打包图片规则
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // 指定图片限制的大小
                            limit: 1024 * 100,
                            // 指定打包后文件名称
                            name: '[name].[ext]',
                            // 指定打包后文件存放目录
                            outputPath: 'images/',
                        }
                    }
                ]
            },
            // 打包CSS规则
            {
                test: /\.css$/,
                /*
                css-loader:   解析css文件中的@import依赖关系
                style-loader: 将webpack处理之后的内容插入到HTML的HEAD代码中
                * */
                // use: [ 'style-loader', 'css-loader' ]
                use:[
                    {
                        loader: "style-loader",
                    },
                    {
                        loader: "css-loader"
                    }
                ]
            },
            // 打包LESS规则
            {
                test: /\.less$/,
                use: [{
                    loader: "style-loader" // creates style nodes from JS strings
                }, {
                    loader: "css-loader" // translates CSS into CommonJS
                }, {
                    loader: "less-loader" // compiles Less to CSS
                }]
            }
        ]
    }
};
```



### scss-loader

自动将scss转换为CSS

```javascript
// npm install --save-dev node-sass

// npm install --save-dev sass-loader

const path = require("path");

module.exports = {
    /*
    配置sourcemap
    development: cheap-module-eval-source-map
    production: cheap-module-source-map
    * */
    devtool: "cheap-module-eval-source-map",
    /*
    mode: 指定打包的模式, 模式有两种
    一种是开发模式(development): 不会对打包的JS代码进行压缩
    还有一种就是上线(生产)模式(production): 会对打包的JS代码进行压缩
    * */
    mode: "development", // "production" | "development"
    /*
    entry: 指定需要打包的文件
    * */
    entry: "./index.js",
    /*
    output: 指定打包之后的文件输出的路径和输出的文件名称
    * */
    output: {
        /*
        filename: 指定打包之后的JS文件的名称
        * */
        filename: "bundle.js",
        /*
        path: 指定打包之后的文件存储到什么地方
        * */
        path: path.resolve(__dirname, "bundle")
    },
    /*
    module: 告诉webpack如何处理webpack不能够识别的文件
    * */
    module: {
        rules: [
            // 打包图片规则
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // 指定图片限制的大小
                            limit: 1024 * 100,
                            // 指定打包后文件名称
                            name: '[name].[ext]',
                            // 指定打包后文件存放目录
                            outputPath: 'images/',
                        }
                    }
                ]
            },
            // 打包CSS规则
            {
                test: /\.css$/,
                /*
                css-loader:   解析css文件中的@import依赖关系
                style-loader: 将webpack处理之后的内容插入到HTML的HEAD代码中
                * */
                // use: [ 'style-loader', 'css-loader' ]
                use:[
                    {
                        loader: "style-loader",
                    },
                    {
                        loader: "css-loader"
                    }
                ]
            },
            // 打包LESS规则
            {
                test: /\.less$/,
                use: [{
                    loader: "style-loader" // creates style nodes from JS strings
                }, {
                    loader: "css-loader" // translates CSS into CommonJS
                }, {
                    loader: "less-loader" // compiles Less to CSS
                }]
            },
            // 打包SCSS规则
            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader" // 将 JS 字符串生成为 style 节点
                }, {
                    loader: "css-loader" // 将 CSS 转化成 CommonJS 模块
                }, {
                    loader: "sass-loader" // 将 Sass 编译成 CSS
                }]
            }
        ]
    }
};
```



### postcss-loader

PostCSS和sass/less不同, 它不是CSS预处理器

PostCSS是一款使用插件去转换CSS的工具，

PostCSS有许多非常好用的插件

例如:

* autoprefixer(自动补全浏览器前缀)

* postcss-pxtorem(自动把px代为转换成rem)

```javascript
const path = require("path");

module.exports = {
    /*
    配置sourcemap
    development: cheap-module-eval-source-map
    production: cheap-module-source-map
    * */
    devtool: "cheap-module-eval-source-map",
    /*
    mode: 指定打包的模式, 模式有两种
    一种是开发模式(development): 不会对打包的JS代码进行压缩
    还有一种就是上线(生产)模式(production): 会对打包的JS代码进行压缩
    * */
    mode: "development", // "production" | "development"
    /*
    entry: 指定需要打包的文件
    * */
    entry: "./index.js",
    /*
    output: 指定打包之后的文件输出的路径和输出的文件名称
    * */
    output: {
        /*
        filename: 指定打包之后的JS文件的名称
        * */
        filename: "bundle.js",
        /*
        path: 指定打包之后的文件存储到什么地方
        * */
        path: path.resolve(__dirname, "bundle")
    },
    /*
    module: 告诉webpack如何处理webpack不能够识别的文件
    * */
    module: {
        rules: [
            // 打包图片规则
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // 指定图片限制的大小
                            limit: 1024 * 100,
                            // 指定打包后文件名称
                            name: '[name].[ext]',
                            // 指定打包后文件存放目录
                            outputPath: 'images/',
                        }
                    }
                ]
            },
            // 打包CSS规则
            {
                test: /\.css$/,
                /*
                css-loader:   解析css文件中的@import依赖关系
                style-loader: 将webpack处理之后的内容插入到HTML的HEAD代码中
                * */
                // use: [ 'style-loader', 'css-loader' ]
                use:[
                    {
                        loader: "style-loader",
                    },
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: "postcss-loader"
                    }
                ]
            },
            // 打包LESS规则
            {
                test: /\.less$/,
                use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader"
                }, {
                    loader: "less-loader"
                }]
            },
            // 打包SCSS规则
            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader"
                }, {
                    loader: "sass-loader"
                }]
            }
        ]
    }
};
```

#### autoprefixer

自动补全浏览器前缀

1. 安装postcss-loader

   > npm i -D postcss-loader
   >

2. 安装需要的插件

   > npm i -D autoprefixer

3. 配置postcss-loader

   在css-loader or less-loader or sass-loader之前添加postcss-loader

4. 创建postcss-loader配置文件

   ```javascript
   // postcss.config,js
   
   module.exports = {
       plugins: {
           "autoprefixer": {
               "overrideBrowserslist": [
                   // "ie >= 8", // 兼容IE7以上浏览器
                   // "Firefox >= 3.5", // 兼容火狐版本号大于3.5浏览器
                   // "chrome  >= 35", // 兼容谷歌版本号大于35浏览器,
                   // "opera >= 11.5" // 兼容欧朋版本号大于11.5浏览器,
                   "chrome  >= 36",
               ]
           }
       }
   };
   ```

#### postcss-pxtorem

自动将px转换成rem

1. 安装postcss-pxtorem

   > npm install postcss-pxtorem -D

2. 在配置文件中配置postcss-pxtorem

   ```javascript
   "postcss-pxtorem": {
       rootValue: 100,// 根元素字体大小
       // propList: ['*'] // 可以从px更改到rem的属性
       propList: ["height"]
   }
   ```

   rootValue (Number) root 元素的字体大小。

   unitPrecision (Number) 允许REM单位增长到的十进制数。

   propList ( array ) 可以从px更改到rem的属性。

     值需要精确匹配。

     使用通配符 * 启用所有属性。 示例：['*']

     在单词的开头或者结尾使用 *。 ( ['*position*'] 将匹配 background-position-y )

     使用 与属性不匹配。! 示例：['*','letter-spacing']!

     将"非"前缀与其他前缀合并。 示例：['*','font*']!

   selectorBlackList ( array ) 要忽略和离开的选择器。

     如果值为字符串，它将检查选择器是否包含字符串。

     ['body'] 将匹配 .body-class

     如果值为 regexp，它将检查选择器是否匹配正则表达式。

     [/^body$/] 将匹配 body，但不匹配 .body

   replace (Boolean) 替代包含rems的规则，而不是添加回退。

   mediaQuery (Boolean) 允许在媒体查询中转换 px。

   minPixelValue (Number) 设置要替换的最小像素值。

```javascript
const path = require("path");

module.exports = {
    /*
    配置sourcemap
    development: cheap-module-eval-source-map
    production: cheap-module-source-map
    * */
    devtool: "cheap-module-eval-source-map",
    /*
    mode: 指定打包的模式, 模式有两种
    一种是开发模式(development): 不会对打包的JS代码进行压缩
    还有一种就是上线(生产)模式(production): 会对打包的JS代码进行压缩
    * */
    mode: "development", // "production" | "development"
    /*
    entry: 指定需要打包的文件
    * */
    entry: "./index.js",
    /*
    output: 指定打包之后的文件输出的路径和输出的文件名称
    * */
    output: {
        /*
        filename: 指定打包之后的JS文件的名称
        * */
        filename: "bundle.js",
        /*
        path: 指定打包之后的文件存储到什么地方
        * */
        path: path.resolve(__dirname, "bundle")
    },
    /*
    module: 告诉webpack如何处理webpack不能够识别的文件
    * */
    module: {
        rules: [
            // 打包图片规则
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // 指定图片限制的大小
                            limit: 1024 * 100,
                            // 指定打包后文件名称
                            name: '[name].[ext]',
                            // 指定打包后文件存放目录
                            outputPath: 'images/',
                        }
                    }
                ]
            },
            // 打包CSS规则
            {
                test: /\.css$/,
                /*
                css-loader:   解析css文件中的@import依赖关系
                style-loader: 将webpack处理之后的内容插入到HTML的HEAD代码中
                * */
                // use: [ 'style-loader', 'css-loader' ]
                use:[
                    {
                        loader: "style-loader",
                    },
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: "postcss-loader"
                    }
                ]
            },
            // 打包LESS规则
            {
                test: /\.less$/,
                use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader"
                }, {
                    loader: "less-loader"
                },{
                    loader: "postcss-loader"
                }]
            },
            // 打包SCSS规则
            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader"
                }, {
                    loader: "sass-loader"
                },{
                    loader: "postcss-loader"
                }]
            }
        ]
    }
};
```



## Plugins

