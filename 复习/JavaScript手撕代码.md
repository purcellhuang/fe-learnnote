### JavaScript编码能力

- 1.多种方式实现数组去重、扁平化、对比优缺点

  ```javascript
  //去重
  const arr = [1, 2, 3, 1, 5, 2]
  
  //1.Set
  console.log([...new Set(arr)]);
  
  //2.遍历
  let result = []
  arr.forEach((item, index) => {
    // if (result.indexOf(item) === -1) {
    //   result.push(item)
    // }
  
    if (!result.includes(item)) {
      result.push(item)
    }
  
    // if (arr.lastIndexOf(item) === index) {
    //   result.push(item)
    // }
  })
  
  console.log(result);
  
  //3.filter
  result = arr.filter((item, index) => {
    return arr.lastIndexOf(item) === index
  })
  
  console.log(result);
  
  
  
  //扁平化
  arr = [[11, 22], 2, 3, [44, 55, [666, 777]]]
  
  //1.flat
  console.log(arr.flat(2))
  
  //2.reduce
  function flat (arr, n = 1) {
    if (Array.isArray(arr) && n >= 0) {
      return arr.reduce((result, item) => {
        return result.concat(flat(item, n - 1))
      }, [])
    }
    return [arr]
  }
  ```

  

- 2.多种方式实现浅拷贝、深拷贝、对比优缺点

  ```javascript
  //浅拷贝
  function shallowClone (target){
      let result = null
      if(Array.isArray(target)){
          result = []
      }else{
          result = {}
      }
      for (let i in target) {
          if (target.hasOwnProperty(i)) {
              result[i] = target[i];
          }
      }
      return result
  } 
  
  shallowClone(obj)
  
  //Object.assign 用于拷贝对象。
  Object.assign({},obj)
  
  //{...obj} [...arr]
  let result = {...obj}
  let list = [...arr]
  
  
  //深拷贝
  //1.借助 JSON.parse(JSON.stringify())
  //	1、不能存放函数或者 Undefined，否则会丢失函数或者 Undefined；
  //	2、不要存放时间对象，否则会变成字符串形式；
  //	3、不能存放 RegExp、Error 对象，否则会变成空对象；
  //	4、不能存放 NaN、Infinity、-Infinity，否则会变成 null；
  JSON.parse(JSON.stringify(obj))
  
  //2.借助第三方库 Lodash、jQuery 等
  
  //3.手写深拷贝
  function deepClone (target, map = new WeakMap()) {
      let result = null
      if (Array.isArray(target)) {
          result = []
      } else {
          result = {}
      }
      // 处理循环依赖
      if (map.get(target)) {
          return map.get(target)
      } else {
          map.set(target, result)
      }
  
      for (let i in target) {
          if (target.hasOwnProperty(i)) {
              if (typeof target[i] === 'object') {
                  result[i] = deepClone(target[i], map)
              } else {
                  result[i] = target[i]
              }
          }
      }
      return result
  }
  
  ```

  

- 3.手写函数柯里化工具函数、并理解其应用场景和优势

  柯里化就是把接受**多个参数**的函数变换成接受一个**单一参数**的函数，并且返回接受**余下参数**返回结果的技术

  ```javascript
  function sum(a, b, c, d) {
      return a + b + c + d;
  }
  // 柯里化函数
  function curry(fn, ...args) {
      // 如果传递的参数还没有达到要执行的函数fn的个数
      // 就继续返回新的函数(高阶函数)
      // 并且返回curry函数传递剩下的参数
      if (args.length < fn.length) {
          return (...newArgs) => curry(fn, ...args, ...newArgs);
      } else {
          return fn(...args);
      }
  }
  
  // 测试用例
  let add = curry(sum);
  console.log(add(1)(2)(3)(4));
  console.log(add(1, 2, 3)(4));
  console.log(add(1, 2)(3, 4));
  console.log(add(1)(2, 3)(4));
  ```

  

- 4.手写防抖和节流工具函数、并理解其内部原理和应用场景

  * 节流

    节流是一定时间内执行一次函数。

    鼠标不断点击触发，mousedown(单位时间内只触发一次)

    监听滚动事件，比如是否滑到底部自动加载更多，用throttle来判断

  * 防抖

    防抖是在一定时间内执行最后一次的函数。

    search搜索联想，用户在不断输入值时，用防抖来节约请求资源。

    window触发resize的时候，不断的调整浏览器窗口大小会不断的触发这个事件，用防抖来让其只触发一次

  ```javascript
  function throttle(){
      
  }
  
  function debounce(){
      
  }
  
  ```

  

- 5.实现一个`sleep`函数

  ```javascript
  function sleep (delay) {
    return new Promise((resolve, reject) => {
      setTimeout(resolve, delay)
    })
  }
  
  //调用
  sleep(1000).then(() => {
    console.log('sleep over')
  })
  
  async function doSomething () {
    await sleep(1000)
    console.log('sleep over')
  }
  
  doSomething()
  ```

  

### 手动实现前端轮子

- 1.手动实现`call、apply、bind`

  ```javascript
  Function.prototype.apply = function(context, args){
      let context = (Object)context || window
      let fn = new Symbol()
      context[fn] = this
      let result = context[fn](...args)
      delete context[fn]
      retuen result 
  }
  
  Function.prototype.call = function(context, ...args){
      let context = (Object)context || window
      let fn = new Symbol()
      context[fn] = this
      let result = context[fn](...args)
      delete context[fn]
      retuen result 
  }
  
  Function.prototype.bind = function(context, ...args){
      return (...newArgs) => {
          return this.call(context, ...args, ...newArgs)
      }
  }
  ```

  

- 2.手动实现符合`Promise/A+`规范的`Promise`、手动实现`async await`

  ```javascript
  
  ```

  

- 3.手写一个`EventEmitter`实现事件发布、订阅 

  ```javascript
  class EventEmitter{
      constructor(){
          this.events = Object.create(null)
      }
      
      on(event, cb){
          if(this.evnets[event]){
              !this.events[event].includes(cb) && this.events[event].push(cb)
          }else{
              this.events[event] = [cb]
          }
      }
      
      emit(event, ...args){
          if(this.events[event]){
              this.events[event].forEach(cb => {
                  cb.call(this, ...args)
              })
          }
      }
      
      off(event, cb){
          if(this.events[event]){
              this.events[event] = this.events[event].filter(listener => {
                  return listener !== cb
              })
          }
      }
  }
  ```

  

- 4.可以说出两种实现双向绑定的方案、可以手动实现

- 5.手写`JSON.stringify`、`JSON.parse`

- 6.手写一个模版引擎，并能解释其中原理

- 7.手写`懒加载`、`下拉刷新`、`上拉加载`、`预加载`等效果