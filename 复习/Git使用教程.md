
## 基本使用

[git教程：https://www.liaoxuefeng.com/wiki/896043488029600](https://www.liaoxuefeng.com/wiki/896043488029600)

[官网地址：https://git-scm.com/](https://git-scm.com/)

## 分支管理

```
git branch //查看当前所有分支

git checkout -b dev  //创建并切换到dev分支  相当于 git branch dev  +  git checkout dev

git branch -d dev  //删除dev分支

git switch -c dev   //创建并切换到dev分支 

git switch master     //切换到master分支

git merge dev  //合并dev到当前分支
```


## 常用指令


```
git log --graph   //可以看到分支合并图

git stash save "msg"   //保存当前工作区内容到暂存区

git stash list    //查看当前暂存区内容

git stash pop  //弹出暂存区顶部的内容

git stash apply stash{0}   //应用stash{0}的内容

git cherry-pick <commit>    //合并commit到当前分支

git remote -v   //查看远程库信息

git reset 

git revert 
```

## Git Flow

### 常用分支

![image-20210409001952162](assets/Git使用教程/image-20210409001952162.png)

### 准备阶段

![image-20210409002051471](assets/Git使用教程/image-20210409002051471.png)



### 开发阶段

![image-20210409002115270](assets/Git使用教程/image-20210409002115270.png)



### 准备上线阶段

![image-20210409002138832](assets/Git使用教程/image-20210409002138832.png)



### 正式上线阶段

![image-20210409002205887](assets/Git使用教程/image-20210409002205887.png)



### 上线之后

![image-20210409002228769](assets/Git使用教程/image-20210409002228769.png)