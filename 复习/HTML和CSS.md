### HTML

- 1.从规范的角度理解`HTML`，从分类和语义的角度使用标签

  语义化是指使⽤恰当语义的html标签，让⻚⾯具有良好的结构与含义，即让正确的标签干正确的事。⽐如 <p> 标签就代表段落， <article> 代表正⽂ 内容等等

  **好处：**

  * 符合W3C规范标准，技术趋势。
  * 阅读友好。增强了可读性。
  * 利于SEO。机器友好。

- 2.常用页面标签的默认样式、自带属性、不同浏览器的差异、处理浏览器兼容问题的方式

- 3.元信息类标签(`head`、`title`、`meta`)的使用目的和配置方法

  > 元信息是指描述自身的信息， 元信息类标签就是指html中用于描述文档自身信息的一类标签，通常出现在head标签中；提供给浏览器或者搜索引擎阅读的，一般不会显示给用户

  **head标签**

  > 规定文档相关的配置信息（元数据），包括文档的标题，引用的文档样式和脚本等。它本身不携带任何内容，主要用于放置其他标签。

  head标签至少包含一个title标签来指定文档的标题，除非是iframe，或者通过其他方式指定了title。且如果没有指定head，大部分浏览器会自动创建一个head标签

  **title标签**

  > title标签用于定义文档的标题，显示在浏览器的**标题栏**或者**标签页**上。作为元信息，可能会被用于浏览器收藏夹、微信推送卡片、微博等各种场景

  **meta标签**

  * charset

    ⽤于描述HTML⽂档的编码形式

    ```html
    <meta charset="UTF-8" > 
    ```

  * http-equiv

    顾名思义，相当于http的⽂件头作⽤,⽐如下⾯的代码就可以设置http的缓存过期⽇期 

    ```html
    <meta http-equiv="expires" content="Wed, 20 Jun 2019 22:33:00 GMT">
    ```

  * viewport

    移动前端最熟悉不过，Web开发⼈员可以控制视⼝的⼤⼩和⽐例 

    ```html
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    ```

  * apple-mobile-web-app-status-bar-style

    开发过PWA应⽤的开发者应该很熟悉，为了⾃定义评估⼯具栏的颜⾊。

    ```html
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"> 
    ```

- 4.`HTML5`离线缓存原理

  **manifest**

  **Server Worker、Caches**

- 5.可以使用`Canvas API`、`SVG`等绘制高性能的动画

### CSS

- 1.`CSS`盒模型，在不同浏览器的差异

  所有HTML元素可以看作盒子，在CSS中，"box model"这一术语是用来设计和布局时使用。

  CSS盒模型本质上是一个盒子，封装周围的HTML元素，它包括：边距，边框，填充，和实际内容。

  可以通过box-sizing进行设置盒子模型

  > box-sizing = content-box  //标准模型
  >
  > box-sizing = border-box  //怪异模型

  * W3C模型（标准模型）

    ![image-20210824145739661](assets/HTML和CSS/image-20210824145739661.png)

  * IE模型（怪异模型）

    ![image-20210824145802917](assets/HTML和CSS/image-20210824145802917.png)

- 2.`CSS`所有选择器及其优先级、使用场景，哪些可以继承，如何运用`at`规则

- 3.`CSS`伪类和伪元素有哪些，它们的区别和实际应用

- 4.`HTML`文档流的排版规则，`CSS`几种定位的规则、定位参照物、对文档流的影响，如何选择最好的定位方式，雪碧图实现原理

- 5.水平垂直居中的方案、可以实现`6`种以上并对比它们的优缺点

  ```css
  //1.块状元素
  .box{
      margin: 0 auto;
  }
  
  //2.定位布局
  .box{
      position: absolute;
      left: 0;
      top: 0;
      right: 0;
      bottom: 0;
      margin: auto;
  }
  
  //3.transform
  .box{
      position: absolute;
      top: 0;
      left: 50%;
      transform: translateX(-50%);
  }
  
  //4.flex布局
  .box{
      display: flex;
      justify-content: center;
      align-item: center;
  }
  ```

  

- 6.`BFC`实现原理，可以解决的问题，如何创建`BFC`

  **块级格式化上下文** 。它是一个独立的渲染区域，只有Block-level box参与， 它规定了内部的Block-level Box如何布局，并且与这个区域外部毫不相干。

  **触发条件：** 

  - 根元素（html）

  - 浮动元素（float不等于none）

  - 绝对定位（position等于absolute或fixed）

  - 行内块状元素（inline-block）

  - overflow != visible

  - flex布局

  - 表格单元格（table-cell）

  最常见的就是`overflow:hidden`、`float:left/right`、`position:absolute`。

  **作用：** 

  - 解决浮动高度坍塌
  - margin合并
  - 防止与浮动元素重叠...

- 7.可使用`CSS`函数复用代码，实现特殊效果

- 8.`PostCSS`、`Sass`、`Less`的异同，以及使用配置，至少掌握一种

- 9.`CSS`模块化方案、如何配置按需加载、如何防止`CSS`阻塞渲染

- 10.熟练使用`CSS`实现常见动画，如渐变、移动、旋转、缩放等等

- 11.`CSS`浏览器兼容性写法，了解不同`API`在不同浏览器下的兼容性情况

- 12.掌握一套完整的响应式布局方案

### 手写

- 1.手写图片瀑布流效果

  `vue`实现的瀑布流：https://gitee.com/purcellhuang/waterfall

- 2.使用`CSS`绘制几何图形（圆形、三角形、扇形、菱形等）

  ```html
  
  ```

  

- 3.使用纯`CSS`实现曲线运动（贝塞尔曲线）

  

- 4.实现常用布局（三栏、圣杯、双飞翼、吸顶），可是说出多种方式并理解其优缺点


